<?php
namespace App\Bitm\SEIP126548\Student;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Student{
    public $id = "";
    public $address = "";
    public $phone_number = "";
    public $blood_group = "";
    public $birth_date = "";
    public $gender = "";
    public $religion = "";
    public $image_name = "";
    public $hobbies = "";
    public $department = "";
    public $semester = "";
    public $deleted_at = "";
    public $host = "localhost";
    public $db_name = "student_information";
    public $user = "root";
    public $pass = "";
    public $connection = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data = ""){
        if(array_key_exists("address", $data) && !empty($data)){
            $this->address = $data['address'];
        }
        if(array_key_exists("phone_number", $data) && !empty($data)){
            $this->phone_number = $data['phone_number'];
        }
        if(array_key_exists("blood_group", $data) && !empty($data)){
            $this->blood_group = $data['blood_group'];
        }
        if(array_key_exists("birth_date", $data) && !empty($data)){
            $this->birth_date = $data['birth_date'];
        }
        if(array_key_exists("gender", $data) && !empty($data)){
            $this->gender = $data['gender'];
        }
        if(array_key_exists("religion", $data) && !empty($data)){
            $this->religion = $data['religion'];
        }
        if(array_key_exists("image", $data) && !empty($data)){
            $this->image_name = $data['image'];
        }
        if(array_key_exists("hobbies", $data) && !empty($data)){
            $this->hobbies = $data['hobbies'];
        }
        if(array_key_exists("department", $data) && !empty($data)){
            $this->department = $data['department'];
        }
        if(array_key_exists("semester", $data) && !empty($data)){
            $this->semester = $data['semester'];
        }
        if(array_key_exists("id", $data) && !empty($data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store(){
        $statement = $this->connection->prepare("update student_information.addresses set phone_number = :phone, address = :address where user_id = :id");
        $statement->execute(array("phone"=>$this->phone_number, "address"=>$this->address, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.others set gender = :gender, religion = :religion, blood_group = :blood where user_id = :id");
        $statement->execute(array("gender"=>$this->gender, "religion"=>$this->religion, "blood"=>$this->blood_group, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.academic_details set department = :department, semester = :semester where user_id = :id");
        $statement->execute(array("department"=>$this->department, "semester"=>$this->semester, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.dates set birth_date = :birth where user_id = :id");
        $statement->execute(array("birth"=>$this->birth_date, "id"=>$this->id));
        if(preg_match("/[.]/", $this->image_name)){
            $statement = $this->connection->prepare("update student_information.images set image_name = :image where user_id = :id");
            $statement->execute(array("image"=>$this->image_name, "id"=>$this->id));
        }
        $statement = $this->connection->prepare("update student_information.user_hobbies set hobbies = :hobbies where user_id = :id");
        $result = $statement->execute(array("hobbies"=>$this->hobbies, "id"=>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Your details updated successfully!</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Sorry your details not updated successfully please try again!</div>");
            header("location: create.php?id=$this->id");
        }
    }
    public function view(){
        $statement = $this->connection->query("select users.id, users.name, users.email, addresses.phone_number, addresses.address, others.gender, others.religion, others.blood_group, academic_details.department, academic_details.semester, dates.birth_date, images.image_name, user_hobbies.hobbies from users left join addresses on users.id = addresses.user_id left join others on users.id = others.user_id left join academic_details on users.id = academic_details.user_id left join dates on users.id = dates.user_id left join images on users.id = images.user_id left join user_hobbies on users.id = user_hobbies.user_id where (users.id =".$this->id." and addresses.user_id =".$this->id." and others.user_id =".$this->id." and academic_details.user_id=".$this->id." and dates.user_id =".$this->id." and images.user_id=".$this->id." and user_hobbies.user_id=".$this->id.")");
        $data = $statement->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
    public function index(){
        $statement = $this->connection->query("select users.id, users.name, users.email, addresses.phone_number, addresses.address, 
others.gender, others.religion, others.blood_group, academic_details.department, 
academic_details.semester, dates.birth_date, images.image_name, user_hobbies.hobbies 
from users left join addresses on users.id = addresses.user_id left join others on 
users.id = others.user_id left join academic_details on users.id = 
academic_details.user_id left join dates on users.id = dates.user_id left join images 
on users.id = images.user_id left join user_hobbies on users.id = 
user_hobbies.user_id  where (academic_details.deleted_at is null and 
addresses.deleted_at is null and dates.deleted_at is null and images.deleted_at is 
null and others.deleted_at is null and  user_hobbies.deleted_at is null)");
        $all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $all_data;
    }
    public function softdelete(){
        date_default_timezone_set("Asia/Dhaka");
        $this->deleted_at = date("d-m-Y h:i A");
        $statement = $this->connection->prepare("update student_information.addresses set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.others set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.academic_details set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.dates set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.images set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.user_hobbies set deleted_at = :date where user_id = :id");
        $result = $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been deleted successfully!</div>");
            header("location: index.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been deleted successfully!</div>");
            header("location: index.php");
        }
    }
    public function trashRecord(){
        $statement = $this->connection->query("select users.id, users.name, users.email, addresses.phone_number, addresses.address, 
others.gender, others.religion, others.blood_group, academic_details.department, 
academic_details.semester, dates.birth_date, images.image_name, user_hobbies.hobbies, user_hobbies.deleted_at
from users left join addresses on users.id = addresses.user_id left join others on 
users.id = others.user_id left join academic_details on users.id = 
academic_details.user_id left join dates on users.id = dates.user_id left join images 
on users.id = images.user_id left join user_hobbies on users.id = 
user_hobbies.user_id where (academic_details.deleted_at is not null and 
addresses.deleted_at is not null and dates.deleted_at is not null and images.deleted_at is not
null and others.deleted_at is not null and  user_hobbies.deleted_at is not null)");
        $all_data = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $all_data;
    }
    public function delete(){
        $statement = $this->connection->prepare("delete from student_information.users where id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.addresses where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.others where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.academic_details where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.dates where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.images where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $statement = $this->connection->prepare("delete from student_information.user_hobbies where user_id = :id");
        $result = $statement->execute(array("id"=>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been deleted successfully!</div>");
            header("location: trashed.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been deleted successfully!</div>");
            header("location: trashed.php");
        }
    }
    public function restore(){
        $this->deleted_at = null;
        $statement = $this->connection->prepare("update student_information.addresses set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.others set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.academic_details set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.dates set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.images set deleted_at = :date where user_id = :id");
        $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        $statement = $this->connection->prepare("update student_information.user_hobbies set deleted_at = :date where user_id = :id");
        $result = $statement->execute(array("date"=>$this->deleted_at, "id"=>$this->id));
        if($result){
            Message::message("<div class='alert alert-success'>Data has been restored successfully!</div>");
            header("location: trashed.php");
        }else{
            Message::message("<div class='alert alert-danger'>Data has not been restored successfully!</div>");
            header("location: trashed.php");
        }
    }
    public function checkImage(){
        $statement = $this->connection->prepare("select image_name from student_information.images where user_id = :id");
        $statement->execute(array("id"=>$this->id));
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
}
?>