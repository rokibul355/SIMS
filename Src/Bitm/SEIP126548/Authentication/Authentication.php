<?php
namespace App\Bitm\SEIP126548\Authentication;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Authentication{
    public $id = "";
    public $email = "";
    public $password = "";
    public $host = "localhost";
    public $db_name = "student_information";
    public $user = "root";
    public $pass = "";
    public $connection = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data = ""){
        if(array_key_exists("email", $data) && !empty($data)){
            $this->email = $data['email'];
        }
        if(array_key_exists("password", $data) && !empty($data)){
            $this->password = $data['password'];
        }
        return $this;
    }
    public function logIn(){
        $statement = $this->connection->query("select id, email, admin from student_information.users where email = "."'".$this->email."'"." and password = "."'".$this->password."'");
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public function logOut(){
        session_destroy();
        header("location: login.php");
    }
}
?>