<?php
namespace App\Bitm\SEIP126548\Users;
use App\Bitm\SEIP126548\Message\Message;
use PDO;
class Users{
    public $id = "";
    public $email = "";
    public $password = "";
    public $name = "";
    public $host = "localhost";
    public $db_name = "student_information";
    public $user = "root";
    public $pass = "";
    public $connection = "";

    public function __construct()
    {
        try{
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }
    }
    public function setData($data = ""){
        if(array_key_exists("name", $data) && !empty($data)){
            $this->name = $data['name'];
        }
        if(array_key_exists("email", $data) && !empty($data)){
            $this->email = $data['email'];
        }
        if(array_key_exists("password", $data) && !empty($data)){
            $this->password = $data['password'];
        }
        return $this;
    }
    public function store(){
        $statement = $this->connection->query("select * from student_information.users where email =". "'".$this->email."'");
        $exist = $statement->fetch();
        if(empty($exist)){
            $statement = $this->connection->prepare("insert into student_information.users (name, email, password) values (:name, :email, :password)");
            $statement->execute(array("name"=>$this->name, "email"=>$this->email, "password"=>$this->password));
            $this->id = $this->connection->lastInsertId();
            $statement = $this->connection->prepare("insert into student_information.addresses (user_id) values (:id)");
            $statement->execute(array("id"=>$this->id));
            $statement = $this->connection->prepare("insert into student_information.user_hobbies (user_id) values (:id)");
            $statement->execute(array("id"=>$this->id));
            $statement = $this->connection->prepare("insert into student_information.dates (user_id) values (:id)");
            $statement->execute(array("id"=>$this->id));
            $statement = $this->connection->prepare("insert into student_information.others (user_id) values (:id)");
            $statement->execute(array("id"=>$this->id));
            $statement = $this->connection->prepare("insert into student_information.images (user_id) values (:id)");
            $statement->execute(array("id"=>$this->id));
            $statement = $this->connection->prepare("insert into student_information.academic_details (user_id) values (:id)");
            $result = $statement->execute(array("id"=>$this->id));
            if($result){
                Message::message("<div class='alert alert-success'>Registration successfully completed!</div>");
                header("location: signup.php");
            }else{
                Message::message("<div class='alert alert-danger'>Registration not success !</div>");
                header("location: signup.php");
            }
        }else{
            Message::message("<div class='alert alert-danger'>This email is already registered !</div>");
            header("location: signup.php");
        }

    }
}
?>