<?php
session_start();
if(!isset($_SESSION['email'])){
    header("location: ../Users/login.php");
}
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Student\Student;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Student();
$obj->setData($_GET);
$data = $obj->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student Information Management System</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SIMS</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="trashed.php"><span class="glyphicon glyphicon-trash"></span> Trashed Record</a></li>
                <li><a href="../Users/logout.php"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-6">
            <h3>Details Information of <?php echo $data['name'];?></h3>
        </div>
        <div class="col-md-2">
            <br>
            <img src="../../../images/<?php echo $data['image_name'];?>" alt="image" width="180" height="170" style="border: 1px solid #D4D4D4;">
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <a href="pdf.php" class="btn btn-primary">Download PDF</a><br><br>
        </div>
        <div class="col-md-2"></div>
    </div>


    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item"><span style="font-weight: bold">ID : </span><?php echo $data['id'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Name : </span><?php echo $data['name'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Email : </span><?php echo $data['email'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Gender : </span><?php echo $data['gender'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Date of Birth : </span><?php echo $data['birth_date'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Phone Number : </span><?php echo $data['phone_number'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Address : </span><?php echo $data['address'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Department : </span><?php echo $data['department'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Semester : </span><?php echo $data['semester'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Religion : </span><?php echo $data['religion'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Blood Group : </span><?php echo $data['blood_group'];?></li>
                <li class="list-group-item"><span style="font-weight: bold">Hobbies : </span><?php echo $data['hobbies'];?></li>
            </ul>
        </div>
        <div class="col-md-2">


        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript">


</script>
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
