<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Message\Message;
use App\Bitm\SEIP126548\Student\Student;
$obj = new Student();
$obj->setData($_GET);
$data = $obj->view();
$var = explode(",", $data['hobbies']);
$data['hobbies']=$var;
//echo "<pre>";
//var_dump($data);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Student Information Management System</title>

  <!-- Bootstrap -->
  <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
<div class="container" style="background-color: #F9F9F9">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <h3>Add Details Information</h3>
    </div>
    <div class="col-md-4"></div>
  </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 form-group" id="message">
            <?php
            if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
                echo Message::message();
            }
            ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <ul class="list-group">
                <li class="list-group-item">Name :  <?php echo $data['name'];?></li>
            </ul>
        </div>
        <div class="col-md-2"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 form-group">
            <ul class="list-group">
                <li class="list-group-item">Email :  <?php echo $data['email'];?></li>
            </ul>
        </div>
        <div class="col-md-2"></div>
    </div>
  <form action="store.php" method="post" enctype="multipart/form-data">
      <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
              <h4>Personal Details</h4>
              <hr>
          </div>
          <div class="col-md-2"></div>
      </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 form-group">
            <label for="address">Address *  </label>
            <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
            <input type="text" id="address" name="address" placeholder="Enter your address" class="form-control" value="<?php echo $data['address']; ?>">
        </div>
        <div class="col-md-2"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-3 form-group">
        <label for="phone_number">Phone Number *  </label>
        <input type="text" id="phone_number" name="phone_number" placeholder="017XXXXXXXX" class="form-control" value="<?php echo $data['phone_number']; ?>">
      </div>
      <div class="col-md-2 form-group">
        <label for="blood_group">Blood Group  </label>
        <input type="text" name="blood_group" id="blood_group" placeholder="Enter blood group" class="form-control" value="<?php echo $data['blood_group']; ?>">
      </div>
      <div class="col-md-3 form-group">
        <label for="birth_date">Date of Birth *  </label>
        <input type="text" name="birth_date" id="birth_date" placeholder="DD-MM-YYYY" class="form-control" value="<?php echo $data['birth_date']; ?>">
      </div>
      <div class="col-md-2"></div>
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8 form-group">
        <label for="">Gender * : </label>
      </div>
      <div class="col-md-2"></div>
    </div>
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-2 form-group">
        <label class="radio-inline" for="male"><input type="radio" name="gender" id="male" value="Male" <?php if(in_array("Male", $data)){echo "checked";}?>>Male</label>
      </div>
      <div class="col-md-2 form-group">
        <label class="radio-inline" for="female"><input type="radio" name="gender" id="female" value="Female" <?php if(in_array("Female", $data)){echo "checked";}?>>Female</label>
      </div>
      <div class="col-md-4"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8 form-group">
        <label for="religion">Religion *  </label>
        <input type="text" id="religion" name="religion" placeholder="Enter your religion" class="form-control" value="<?php echo $data['religion']; ?>">
      </div>
      <div class="col-md-2"></div>
    </div>
    <div class="row">
      <div class="col-md-2">
      </div>
      <div class="col-md-2 form-group"><label for="">Upload Photo *</label></div>
      <div class="col-md-4 form-group">
        <input type="file" name="image">
      </div>
      <div class="col-md-4">
          <img src="../../../images/<?php echo $data['image_name']?>" alt="<?php $data['image_name']?>" width="150" height="100">
      </div>
    </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8 form-group">
        <label for="">Select your Hobbies *  </label>
      </div>
      <div class="col-md-2"></div>
    </div>
   <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-6 form-group" style="overflow-x: hidden; overflow-y: scroll; height: 200px; border: 1px solid #D4D4D4">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="coding" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Coding" id="coding" <?php if(in_array("Coding", $data['hobbies'])){echo "checked";}?>>Coding</label>
          </div>
          <div class="col-md-4">
            <label for="gardening" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="gardening" value="Gardening" <?php if(in_array("Gardening", $data['hobbies'])){echo "checked";}?>>Gardening</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="watching_movie" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Watching Movie" id="watching_movie" <?php if(in_array("Watching Movie", $data['hobbies'])){echo "checked";}?>>Watching Movie</label>
          </div>
          <div class="col-md-4">
            <label for="reading_book" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="reading_book" value="Reading Book" <?php if(in_array("Reading", $data['hobbies'])){echo "checked";}?>>Reading Book</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="cricket" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Cricket" id="cricket" <?php if(in_array("Cricket", $data['hobbies'])){echo "checked";}?>>Cricket</label>
          </div>
          <div class="col-md-4">
            <label for="football" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="football" value="Football" <?php if(in_array("Football", $data['hobbies'])){echo "checked";}?>>Football</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="swimming" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Swimming" id="swimming" <?php if(in_array("Swimming", $data['hobbies'])){echo "checked";}?>>Swimming</label>
          </div>
          <div class="col-md-4">
            <label for="driving" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="driving" value="Driving" <?php if(in_array("Driving", $data['hobbies'])){echo "checked";}?>>Driving</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="travelling" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Travelling" id="travelling" <?php if(in_array("Travelling", $data['hobbies'])){echo "checked";}?>>Travelling</label>
          </div>
          <div class="col-md-4">
            <label for="painting" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="painting" value="Painting" <?php if(in_array("Painting", $data['hobbies'])){echo "checked";}?>>Painting</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="martial_arts" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Martial Arts" id="martial_arts" <?php if(in_array("Martial Arts", $data['hobbies'])){echo "checked";}?>>Martial Arts</label>
          </div>
          <div class="col-md-4">
            <label for="fishing" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="fishing" value="Fishing" <?php if(in_array("Fishing", $data['hobbies'])){echo "checked";}?>>Fishing</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="drawing" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Drawing" id="drawing" <?php if(in_array("Drawing", $data['hobbies'])){echo "checked";}?>>Drawing</label>
          </div>
          <div class="col-md-4">
            <label for="dancing" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="dancing" value="Dancing" <?php if(in_array("Dancing", $data['hobbies'])){echo "checked";}?>>Dancing</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="chess" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Chess" id="chess" <?php if(in_array("Chess", $data['hobbies'])){echo "checked";}?>>Chess</label>
          </div>
          <div class="col-md-4">
            <label for="car_racing" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="car_racing" value="Car Racing" <?php if(in_array("Car Racing", $data['hobbies'])){echo "checked";}?>>Car Racing</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="basketball" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Basketball" id="basketball" <?php if(in_array("Basketball", $data['hobbies'])){echo "checked";}?>>Basketball</label>
          </div>
          <div class="col-md-4">
            <label for="badminton" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="badminton" value="Badminton" <?php if(in_array("Badminton", $data['hobbies'])){echo "checked";}?>>Badminton</label>
          </div>
          <div class="col-md-2"></div>
        </div>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <label for="eating" class="checkbox-inline"><input type="checkbox" name="hobbies[]" value="Eating" id="eating" <?php if(in_array("Eating", $data['hobbies'])){echo "checked";}?>>Eating</label>
          </div>
          <div class="col-md-4">
            <label for="shopping" class="checkbox-inline"><input type="checkbox" name="hobbies[]" id="shopping" value="Shopping" <?php if(in_array("Shopping", $data['hobbies'])){echo "checked";}?>>Shopping</label>
          </div>
          <div class="col-md-2"></div>
        </div>
      </div>
        <div class="col-md-2"></div>
    </div>
      <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
              <h4>Academic Details</h4>
              <hr>
          </div>
          <div class="col-md-2"></div>
      </div>
      <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 form-group">
              <label for="department">Department *  </label>
              <input type="text" id="department" name="department" placeholder="Enter your department" class="form-control" value="<?php echo $data['department']; ?>">
          </div>
          <div class="col-md-2"></div>
      </div>
      <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8 form-group">
              <label for="semester">Semester *  </label>
              <input type="text" id="semester" name="semester" placeholder="Enter your semester" class="form-control" value="<?php echo $data['semester']; ?>">
          </div>
          <div class="col-md-2"></div>
      </div>
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-7">

      </div>
      <div class="col-md-3">
          <input type="submit" value="Save Changes" class="btn btn-success">
      </div>
    </div>
  </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
</body>
</html>