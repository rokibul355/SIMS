<?php 
include("../../../vendor/autoload.php");
include("../../../vendor/mpdf/mpdf/mpdf.php");
use App\Bitm\SEIP126548\Student\Student;
$obj = new Student();

$obj->setData($_GET);
$data = $obj->view();

$list = "";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>ID : </span> $data[id]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Name : </span> $data[name]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Email : </span> $data[email]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Gender : </span> $data[gender]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Date of Birth : </span> $data[birth_date]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Phone Number : </span> $data[phone_number]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Address : </span> $data[address]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Department : </span> $data[department]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Semester : </span> $data[semester]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Religion : </span> $data[religion]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Blood Group : </span> $data[blood_group]</li>";
$list.="<li class='list-group-item' style='list-style: none'><span style='font-weight: bold'>Hobbies : </span> $data[hobbies]</li>";



$html = <<<HDOC
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mobile Project</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<style type="text/css">
		th,td{text-align:center}
		
	</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-6">
            <h3 style="text-align: center">Details Information of $data[name]</h3>
        </div>
        <div class="col-md-2">
            <br>
            <img src="../../../images/'.$inputPath.'" alt="image" width="180" height="170" style="border: 1px solid #D4D4D4;">
        </div>
    </div>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
			<ul class="list-group">
                $list
            </ul>
		</div>
		<div class="col-md-1">
		</div>
	</div>
</div>	
</body>
</html>	
HDOC;
$mpdf = new Mpdf();
//$mpdf->showImageErrors = true;
$mpdf->WriteHTML($html);
$mpdf->Output();
?>