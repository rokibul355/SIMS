<?php
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Student\Student;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Student();
$all_data = $obj->index();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student Information Management System</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SIMS</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="../../../index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="../Users/signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="../Users/login.php"><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h3> All Information </h3>
        </div>
        <div class="col-md-4"></div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <table class="table table-bordered">
                <thead>
                <tr class="success">
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Department</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach($all_data as $data) {
                    $i++;
                    ?>
                    <tr>
                        <td class="warning"><?php echo $i; ?></td>
                        <td class="warning"><?php echo $data['id'];?></td>
                        <td class="warning"><?php echo $data['name'];?></td>
                        <td class="warning"><?php echo $data['email'];?></td>
                        <td class="warning"><?php echo $data['department'];?></td>
                        <td class="warning" width="12%">
                            <a href="view.php?id=<?php echo $data['id']; ?>" class="btn btn-info">View Details</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-1">


        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
