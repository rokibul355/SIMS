<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Message\Message;
use App\Bitm\SEIP126548\Student\Student;
$obj = new Student();

if($_SERVER['REQUEST_METHOD'] == "POST"){
    if(!empty($_POST['address']) && !empty($_POST['phone_number']) && !empty($_POST['birth_date']) && !empty($_POST['gender']) && !empty($_POST['religion']) && !empty($_POST['hobbies']) && !empty($_POST['department']) && !empty($_POST['semester']) ){
        if(!preg_match("/^[0-9]{11}$/", $_POST['phone_number'])){
            Message::message("<div class='alert alert-danger'>Invalid Phone Number !</div>");
            header("location: create.php?id=$_POST[id]");
        }elseif(!preg_match("/^[a-zA-Z ]*$/", $_POST['religion'])){
            Message::message("<div class='alert alert-danger'>Invalid Religion!</div>");
            header("location: create.php?id=$_POST[id]");
        }elseif(!preg_match("/^[a-zA-Z ]*$/", $_POST['department'])){
            Message::message("<div class='alert alert-danger'>Invalid Department!</div>");
            header("location: create.php?id=$_POST[id]");
        }elseif(!preg_match("/^[a-zA-Z0-9 ]*$/", $_POST['semester'])){
            Message::message("<div class='alert alert-danger'>Invalid Semester !</div>");
            header("location: create.php?id=$_POST[id]");
        }elseif(!preg_match("/^[a-zA-Z0-9 .\/-]*$/", $_POST['birth_date'])){
            Message::message("<div class='alert alert-danger'>Invalid Date of Birth !</div>");
            header("location: create.php?id=$_POST[id]");
        }elseif(!preg_match("/^[a-zA-Z +-]*$/", $_POST['blood_group'])){
            Message::message("<div class='alert alert-danger'>Invalid Blood Group !</div>");
            header("location: create.php?id=$_POST[id]");
        }else{
            $_POST['image'] = time().$_FILES['image']['name'];
            $temp_location = $_FILES['image']['tmp_name'];
            $destination = "../../../images/";
            move_uploaded_file($temp_location, $destination.$_POST['image']);
            $var = implode(",", $_POST['hobbies']);
            $_POST['hobbies'] = $var;
            $_POST['address'] = filter_var($_POST['address'], FILTER_SANITIZE_STRING);

            $obj->setData($_POST);
            $data = $obj->checkImage();
            if(!preg_match("/[.]/", $_POST['image']) && empty($data['image_name'])){
                Message::message("<div class='alert alert-danger'>Please fill up the required fields !</div>");
                header("location: create.php?id=$_POST[id]");
            }elseif(preg_match("/[.]/", $_POST['image']) && empty($data['image_name'])){
                $obj->store();
            }elseif(!empty($data['image_name']) && preg_match("/[.]/", $_POST['image'])){
                $data = $obj->view();
                $data['image_name'] = $_SERVER['DOCUMENT_ROOT']."/StudentInformation/images/".$data['image_name'];
                unlink($data['image_name']);
                $obj->store();
            }elseif(!empty($data['image_name']) && !preg_match("/[.]/", $_POST['image'])){
                $obj->store();
            }
        }

    }else{
        Message::message("<div class='alert alert-danger'>Please fill up the required fields !</div>");
        header("location: create.php?id=$_POST[id]");
    }
}else{
    header("location: create.php");
}
?>
