<?php
session_start();
if(!isset($_SESSION['email'])){
    header("location: ../Users/login.php");
}
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Student\Student;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Student();
if(isset($_GET['id'])){
    $obj->setData($_GET);
    $data = $obj->softdelete();
}else{
    header("location: index.php");
}
?>