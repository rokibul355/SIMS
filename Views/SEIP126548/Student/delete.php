<?php
session_start();
if(!isset($_SESSION['email'])){
    header("location: ../Users/login.php");
}
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Student\Student;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Student();
if(isset($_GET['id'])){
    $obj->setData($_GET);
    $data = $obj->view();
    $data['image_name'] = $_SERVER['DOCUMENT_ROOT']."/StudentInformation/images/".$data['image_name'];
    unlink($data['image_name']);
    $obj->delete();
}else{
    header("location: trashed.php");
}
?>