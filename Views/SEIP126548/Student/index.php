<?php
session_start();
if(!isset($_SESSION['email'])){
    header("location: ../Users/login.php");
}
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Student\Student;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Student();
$all_data = $obj->index();
if(isset($_GET['id'])){
    $_SESSION['id'] = $_GET['id'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student Information Management System</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Student Information Management System</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="../Users/logout.php"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h3>List of All Information </h3>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6" id="message">
            <?php
            if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){

                echo Message::message();
            }
            ?>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            <a href="pdf.php" class="btn btn-primary">Download PDF</a>
            <a href="trashed.php" class="btn btn-warning">Download Excel</a><br><br>
        </div>
        <div class="col-md-1"></div>
    </div>


    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <table class="table table-bordered">
                <thead>
                <tr class="success">
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Department</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach($all_data as $data) {
                        $i++;
                        ?>
                        <tr>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>"><?php echo $i; ?></td>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>"><?php echo $data['id'];?></td>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>"><?php echo $data['name'];?></td>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>"><?php echo $data['email'];?></td>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>"><?php echo $data['department'];?></td>
                            <td class="<?php if($data['id'] == $_SESSION['id']){echo "active";}else{echo "warning";}?>" width="26%">
                                <a href="view.php?id=<?php echo $data['id']; ?>" class="btn btn-info">View</a>
                                <a href="<?php if($data['id'] == $_SESSION['id']){echo "create.php?id=$data[id]";}else{echo " ";}?>" class="btn btn-primary" <?php if($data['id'] != $_SESSION['id']){echo "disabled";}?>>Set Details</a>
                                <a href="<?php if($data['id'] == $_SESSION['id']){echo "soft_delete.php?id=$data[id]";}else{echo " ";}?>" class="btn btn-danger" <?php if($data['id'] != $_SESSION['id']){echo "disabled";}?>
                                    <?php if($data['id'] == $_SESSION['id']){echo "onclick='return confirm_delete();'";}?> >Delete</a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-1">


        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script type="text/javascript">
    $("#message").show().delay(3000).fadeOut();

    function confirm_delete(){
        var ok = confirm("Are you sure want to delete your record?");
        if(ok){
            return true;
        }else{
            return false;
        }
    }

</script>
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
