<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Message\Message;
use App\Bitm\SEIP126548\Users\Users;
$obj = new Users();
if($_SERVER['REQUEST_METHOD'] == "POST"){
    if(!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirm_password'])){
        if(!preg_match("/^[a-zA-Z .]*$/", $_POST['name'])){
            Message::message("<div class='alert alert-danger'>Invalid Name !</div>");
            header("location: signup.php");
        }elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            Message::message("<div class='alert alert-danger'>Invalid Email address !</div>");
            header("location: signup.php");
        }elseif($_POST['password'] !== $_POST['confirm_password']){
            Message::message("<div class='alert alert-danger'>Both password doesn't match !</div>");
            header("location: signup.php");
        }else{
            $_POST['name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
            $_POST['email'] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $_POST['password'] = md5($_POST['password']);
            $obj->setData($_POST);
            $obj->store();
        }
    }else{
        Message::message("<div class='alert alert-danger'>All the fields are required! Please fill up all the fields</div>");
        header("location: signup.php");
    }
}else{
    header("location: signup.php");
}
?>