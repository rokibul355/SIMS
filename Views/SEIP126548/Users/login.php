<?php
session_start();
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP126548\Message\Message;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student Information Management System</title>

    <link rel="stylesheet" href="../../../bootstrap/css/bootstrap.min.css">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SIMS</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="../../../index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="../Information/index.php"><span class="glyphicon glyphicon-book"></span> Information</a></li>
                <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container" style="margin-top: 5%; background-color: #e5e5e5; padding-bottom: 5%">

    <div class="row" style="margin-left: 5%; margin-right: 5%;">

        <div class="page-header" >
            <h2>Log In</h2>
            <div id="message" style="text-align: center;">
                <?php
                if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
                    echo Message::message();
                }
                ?>
            </div>
        </div>

        <form method="post" action="login_process.php">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter your email address" name="email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Enter your password" name="password">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-lg btn-success" value="Login">
                </div>
        </form>
            <p> If you are not registered, please <a href="signup.php">sign up</a> first. </p>
    </div>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
</body>
</html>
