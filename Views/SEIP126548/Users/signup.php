<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Message\Message;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Student Information Management System</title>

    <!-- Bootstrap -->
    <link href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SIMS</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="../../../index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="../Information/index.php"><span class="glyphicon glyphicon-book"></span> Information</a></li>
                <li><a href="../Users/login.php"><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="background-color: #F9F9F9; margin-top: 50px">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <h3>Registration Form</h3>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <p>If you are already registered you can <a href="login.php">Log in</a>.No need to registration again.</p>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 form-group" id="message">
            <?php
            if(array_key_exists("message", $_SESSION) && !empty($_SESSION['message'])){
                echo Message::message();
            }
            ?>
        </div>
        <div class="col-md-2"></div>
    </div>
    <form action="store.php" method="post" >
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-group">
                <label for="name">Name *  </label>
                <input type="text" id="name" name="name" placeholder="Enter your name" class="form-control">
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-group">
                <label for="email">Email *  </label>
                <input type="email" id="email" name="email" placeholder="Enter your email" class="form-control">
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4 form-group">
                <label for="password">Password *  </label>
                <input type="password" id="password" name="password" placeholder="*****************" class="form-control">
            </div>
            <div class="col-md-4 form-group">
                <label for="confirm_password">Confirm Password *  </label>
                <input type="password" id="confirm_password" name="confirm_password" placeholder="*****************" class="form-control">
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 form-group" style="text-align: right">
                <input type="submit" value="Sign Up" class="btn btn-success">
            </div>
            <div class="col-md-2"></div>
        </div>
    </form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../bootstrap/js/bootstrap.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
</body>
</html>