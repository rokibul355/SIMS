<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Message\Message;
use App\Bitm\SEIP126548\Authentication\Authentication;
$obj = new Authentication();
if($_SERVER['REQUEST_METHOD'] == "POST"){
    $_POST['password'] = md5($_POST['password']);
    $obj->setData($_POST);
    $result = $obj->logIn();
    if($result){
        $_SESSION['email'] = $result['email'];
        if($result['admin']== "yes"){
            header("location: ../Admin/index.php");
        }else{
            header("location: ../Student/index.php?id=$result[id]");
        }
    }else{
        Message::message("<div class='alert alert-danger'>Invalid Email or Password!</div>");
        header("location: login.php");
    }
}else{
    header("location: signup.php");
}
?>