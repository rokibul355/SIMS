<?php
session_start();
if(!isset($_SESSION['email'])){
    header("location: login.php");
}
include("../../../vendor/autoload.php");
use App\Bitm\SEIP126548\Authentication\Authentication;
use App\Bitm\SEIP126548\Message\Message;
$obj = new Authentication();
$obj->logOut();
?>